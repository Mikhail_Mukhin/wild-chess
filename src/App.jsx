import React, { useState } from "react";
import { useDispatch } from "react-redux";
import Board from "./components/Board";
import "./style/main.scss";
import ActionsWithFigures from "./components/ActionsWithFigures";
import ActionModal from "./components/ActionModal";

function App() {
  const [player, setPlayer] = useState("white");
  const [endTurn, setEndTurn] = useState(false);
  const [notification, setNotification] = useState(false);
  const [showActionModal, setShowActionModal] = useState(true);
  const [rules, setRules] = useState(false);
  const [whoseTurn, setWhoseTurn] = useState(true);

  const dispatch = useDispatch();

  const changeModalContent = ({
    transitionPlayerTurn,
    showModal,
    notification,
    endTurn,
    whoseTurn,
    changeRule
  }) => {
    setEndTurn(endTurn);
    setWhoseTurn(whoseTurn);
    setRules(changeRule);

    if (transitionPlayerTurn) {
      setPlayer(player === "white" ? "black" : "white");
      dispatch({ type: "saveDiceNumber", payload: null });
    }

    if (!notification) {
      setNotification(notification);
    }

    if (showModal !== undefined) {
      setShowActionModal(showModal);
    }
  };

  const notificationMessage = (message) => {
    setNotification(message);
    setShowActionModal(true);
  };

  const rulesGame = () => {
    setRules(true);
    setShowActionModal(true);
  };

  return (
    <div className="app">
      <Board player={player} openActionModal={changeModalContent} />
      <ActionsWithFigures
        notificationMessage={notificationMessage}
        rulesGame={rulesGame}
        player={player}
        turn={player}
      />
      <ActionModal
        showActionModal={showActionModal}
        rules={rules}
        changeModal={changeModalContent}
        notification={notification}
        player={player}
        whoseTurn={whoseTurn}
        endTurn={endTurn}
      />
    </div>
  );
}

export default App;
