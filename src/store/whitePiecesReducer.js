const defaultState = {
  whitePieces: []
};

export const whitePiecesReducer = (state = defaultState, action) => {
  switch (action.type) {
    case "addWhitePieces":
      const computeWeight = () => {
        if (action.payload.pieces === "whitePawn") {
          return 1;
        }
        if (
          action.payload.pieces === "whiteKnight" ||
          action.payload.pieces === "whiteBishop"
        ) {
          return 3;
        }
        if (action.payload.pieces === "whiteRook") {
          return 5;
        }
        if (action.payload.pieces === "whiteQueen") {
          return 9;
        }
        if (action.payload.pieces === "whiteKing") {
          return 12;
        }
      };
      return {
        ...state,
        whitePieces: [
          ...state.whitePieces,
          {
            id:
              new Date().getTime() +
              Math.floor(100000000 + Math.random() * 900000000),
            name: [action.payload.pieces],
            coordinates: action.payload.coordinates || { i: 0, k: 0 },
            color: "white",
            weight: computeWeight(),
            selected: false,
            numberedCell:
              action.payload.numberedCell !== undefined
                ? action.payload.numberedCell
                : 0
          }
        ]
      };
    case "changeSelectedWhitePieces":
      const chengedSelected = [...state.whitePieces].map((pieces) =>
        pieces.id === action.payload
          ? { ...pieces, selected: !pieces.selected }
          : { ...pieces }
      );
      return {
        ...state,
        whitePieces: chengedSelected
      };

    case "resetSelectedWhitePieces":
      const resetSelected = [...state.whitePieces].map((pieces) => ({
        ...pieces,
        selected: false
      }));
      return {
        ...state,
        whitePieces: resetSelected
      };
    case "removeWhitePieces":
      return {
        ...state,
        whitePieces: [
          ...state.whitePieces.filter((pieces) => {
            return (
              pieces.selected !== true &&
              pieces.id !== action.payload?.currentPieces.id
            );
          })
        ]
      };
    default:
      return state;
  }
};
