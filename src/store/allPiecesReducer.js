import {
  faChessPawn,
  faChessKnight,
  faChessBishop,
  faChessRook,
  faChessQueen,
  faChessKing
} from "@fortawesome/free-solid-svg-icons";

const defaultState = {
  chessPieces: {
    blackPawn: faChessPawn,
    whitePawn: faChessPawn,
    blackKnight: faChessKnight,
    whiteKnight: faChessKnight,
    blackBishop: faChessBishop,
    whiteBishop: faChessBishop,
    blackRook: faChessRook,
    whiteRook: faChessRook,
    blackQueen: faChessQueen,
    whiteQueen: faChessQueen,
    blackKing: faChessKing,
    whiteKing: faChessKing
  }
};

export const allPiecesReducer = (state = defaultState, action) => {
  switch (action.type) {
    default:
      return state;
  }
};
