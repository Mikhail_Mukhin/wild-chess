const defaultState = {
  blackPieces: []
};

export const blackPiecesReducer = (state = defaultState, action) => {
  switch (action.type) {
    case "addBlackPieces":
      const computeWeight = () => {
        if (action.payload.pieces === "blackPawn") {
          return 1;
        }
        if (
          action.payload.pieces === "blackKnight" ||
          action.payload.pieces === "blackBishop"
        ) {
          return 3;
        }
        if (action.payload.pieces === "blackRook") {
          return 5;
        }
        if (action.payload.pieces === "blackQueen") {
          return 9;
        }
        if (action.payload.pieces === "blackKing") {
          return 12;
        }
      };

      return {
        ...state,
        blackPieces: [
          ...state.blackPieces,
          {
            id:
              new Date().getTime() +
              Math.floor(100000000 + Math.random() * 900000000),
            name: [action.payload.pieces],
            coordinates: action.payload.coordinates || { i: 7, k: 7 },
            color: "black",
            weight: computeWeight(),
            selected: false,
            numberedCell:
              action.payload.numberedCell !== undefined
                ? action.payload.numberedCell
                : 14
          }
        ]
      };
    case "changeSelectedBlackPieces":
      const chengedSelected = [...state.blackPieces].map((pieces) =>
        pieces.id === action.payload
          ? { ...pieces, selected: !pieces.selected }
          : { ...pieces }
      );
      return {
        ...state,
        blackPieces: chengedSelected
      };
    case "resetSelectedBlackPieces":
      const resetSelected = [...state.blackPieces].map((pieces) => ({
        ...pieces,
        selected: false
      }));
      return {
        ...state,
        blackPieces: resetSelected
      };
    case "removeBlackPieces":
      return {
        ...state,
        blackPieces: [
          ...state.blackPieces.filter(
            (pieces) =>
              pieces.selected !== true &&
              pieces.id !== action.payload?.currentPieces.id
          )
        ]
      };
    default:
      return state;
  }
};
