const defaultSatte = {
  diceNumber: null
};

export const diceNumberReducer = (state = defaultSatte, action) => {
  switch (action.type) {
    case "saveDiceNumber":
      return {
        ...state,
        diceNumber: action.payload
      };
    default:
      return state;
  }
};
