const defaultState = {
  activeChengePieces: false
};

export const changePiecesReducer = (state = defaultState, action) => {
  switch (action.type) {
    case "chengePieces":
      return {
        ...state,
        activeChengePieces:
          action.payload !== undefined
            ? action.payload
            : !state.activeChengePieces
      };
    default:
      return state;
  }
};
