import { createStore, combineReducers } from "redux";
import { whitePiecesReducer } from "./whitePiecesReducer";
import { blackPiecesReducer } from "./blackPiecesReducer";
import { allPiecesReducer } from "./allPiecesReducer";
import { changePiecesReducer } from "./changePiecesReducer";
import { diceNumberReducer } from "./diceNumberReducer";

const rootReduser = combineReducers({
  whitePieces: whitePiecesReducer,
  blackPieces: blackPiecesReducer,
  allPieces: allPiecesReducer,
  changePieces: changePiecesReducer,
  diceNumber: diceNumberReducer
});

export const store = createStore(rootReduser);
