import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "../style/main.scss";
import GetDiceRumber from "./GetDiceRumber";
import { ComputeRemainingWeight } from "./methods/ComputeRemainingWeight";
import { NoTrap } from "./methods/NoTrap";

export default function Board(props) {
  const { openActionModal, player } = props;
  const dispatch = useDispatch();
  const allPieces = useSelector((state) => state.allPieces.chessPieces);
  const whitePieces = useSelector((state) => state.whitePieces);
  const blackPieces = useSelector((state) => state.blackPieces);
  const chengePieces = useSelector(
    (state) => state.changePieces.activeChengePieces
  );

  const diceNumber = useSelector((state) => state.diceNumber.diceNumber);
  const board = [];
  const numberedLine = ["1", "2", "3", "4", "5", "6", "7", "8"];
  const [currentPieces, setCurrentPieces] = useState({});
  const piecesOnThisCell = (coordinates) => {
    const allPiecesOnCell = [
      ...whitePieces.whitePieces.filter(
        (pieces) =>
          pieces.coordinates.i === coordinates.i &&
          pieces.coordinates.k === coordinates.k
      ),
      ...blackPieces.blackPieces.filter(
        (pieces) =>
          pieces.coordinates.i === coordinates.i &&
          pieces.coordinates.k === coordinates.k
      )
    ];

    return allPiecesOnCell;
  };

  const clickByImage = (id) => {
    if (chengePieces) {
      if (player === "white") {
        whitePieces.whitePieces.map((pieces) => {
          if (pieces.id === id) {
            dispatch({
              type: "changeSelectedWhitePieces",
              payload: id
            });
          }
        });
      }
      if (player === "black") {
        blackPieces.blackPieces.map((pieces) => {
          if (pieces.id === id) {
            dispatch({
              type: "changeSelectedBlackPieces",
              payload: id
            });
          }
        });
      }
    }
  };

  const areSelectedFigures = () => {
    if (player === "white") {
      return whitePieces.whitePieces.some((pieces) => pieces.selected);
    } else {
      return blackPieces.blackPieces.some((pieces) => pieces.selected);
    }
  };

  const dragOverHandler = (e) => {
    e.preventDefault();
  };

  const dragStartHandler = (e, { i, k, pieces, numberedCell }) => {
    if (pieces) {
      setCurrentPieces({ ...pieces, startNumberedCell: numberedCell });
    }
  };

  const canBeMove = (numberedCell) => {
    if (currentPieces.startNumberedCell + diceNumber > 27) {
      return (
        diceNumber - (27 - currentPieces.startNumberedCell) - 1 === numberedCell
      );
    }
    return currentPieces.startNumberedCell + diceNumber === numberedCell;
  };

  const dropPiecesHandler = (e, { i, k, numberedCell }) => {
    e.preventDefault();
    const was = { delete: false };

    if (
      numberedCell !== undefined &&
      canBeMove(numberedCell) &&
      NoTrap(player, whitePieces, blackPieces, currentPieces, diceNumber)
    ) {
      dispatch({
        type: player === "white" ? "removeWhitePieces" : "removeBlackPieces",
        payload: {
          currentPieces: currentPieces
        }
      });

      if (player === "white") {
        blackPieces.blackPieces.forEach((pieces) => {
          if (pieces.numberedCell === numberedCell) {
            if (pieces.weight <= currentPieces.weight) {
              dispatch({
                type: "removeBlackPieces",
                payload: {
                  currentPieces: pieces
                }
              });
            }
            if (pieces.weight > currentPieces.weight) {
              dispatch({
                type: "removeWhitePieces",
                payload: {
                  currentPieces: currentPieces
                }
              });
              dispatch({
                type: "removeBlackPieces",
                payload: {
                  currentPieces: pieces
                }
              });
              ComputeRemainingWeight(
                pieces.weight - currentPieces.weight,
                player,
                dispatch
              );
              was.delete = true;
            }
          }
        });
      }

      if (player === "black") {
        whitePieces.whitePieces.forEach((pieces) => {
          if (pieces.numberedCell === numberedCell) {
            if (pieces.weight <= currentPieces.weight) {
              dispatch({
                type: "removeWhitePieces",
                payload: {
                  currentPieces: pieces
                }
              });
            }

            if (pieces.weight > currentPieces.weight) {
              dispatch({
                type: "removeBlackPieces",
                payload: {
                  currentPieces: currentPieces
                }
              });
              dispatch({
                type: "removeWhitePieces",
                payload: {
                  currentPieces: pieces
                }
              });
              ComputeRemainingWeight(
                pieces.weight - currentPieces.weight,
                player,
                dispatch
              );
              was.delete = true;
            }
          }
        });
      }
      if (!was.delete) {
        dispatch({
          type: player === "white" ? "addWhitePieces" : "addBlackPieces",
          payload: {
            pieces: currentPieces.name.toString(),
            coordinates: { i, k },
            numberedCell: numberedCell
          }
        });
      }
    }
  };

  for (let i = 0; i < numberedLine.length; i++) {
    for (let k = 0; k < numberedLine.length; k++) {
      if (i === 0) {
        board.push({ i, k, numberedCell: k });
      } else if (k === 7) {
        board.push({ i, k, numberedCell: 7 + i });
      } else if (i === 7 && k !== 7) {
        board.push({ i, k, numberedCell: 21 - k });
      } else if (k === 0 && i !== 0 && i !== 7) {
        board.push({ i, k, numberedCell: 28 - i });
      } else {
        board.push({ i, k });
      }
    }
  }

  return (
    <>
      <GetDiceRumber player={player} />
      <div id="board">
        {board.map((cell, index) => (
          <div
            onDragOver={(e) => dragOverHandler(e)}
            onDragStart={(e) =>
              dragStartHandler(e, {
                i: cell.i,
                k: cell.k,
                pieces: null,
                numberedCell: cell.numberedCell
              })
            }
            onDrop={(e) =>
              dropPiecesHandler(e, {
                i: cell.i,
                k: cell.k,
                numberedCell: cell.numberedCell
              })
            }
            className={`Board-cell ${
              (!(cell.i % 2) && cell.k % 2) || (cell.i % 2 && !(cell.k % 2))
                ? "light-cell"
                : "dark-cell"
            }`}
            key={`${numberedLine[cell.i]}_${numberedLine[cell.k]}`}
          >
            <div
              className={
                (cell.i === 0 && cell.k === 0) || (cell.i === 7 && cell.k === 7)
                  ? "start-cell"
                  : "numbered-cell"
              }
              key={`${cell.i} ${cell.k}`}
            >
              {piecesOnThisCell({
                i: cell.i,
                k: cell.k,
                index: index
              }).length
                ? piecesOnThisCell({
                    i: cell.i,
                    k: cell.k
                  }).map((pieces) => (
                    <div
                      draggable={
                        !areSelectedFigures() && pieces.color === player
                      }
                      onDragStart={(e) =>
                        dragStartHandler(e, {
                          i: cell.i,
                          k: cell.k,
                          pieces: pieces,
                          numberedCell: cell.numberedCell
                        })
                      }
                      key={`${pieces.id}`}
                    >
                      <FontAwesomeIcon
                        className={["chess-pieces", pieces.id].join(" ")}
                        onClick={() => clickByImage(pieces.id, pieces.name)}
                        style={{
                          cursor: chengePieces ? "pointer" : "grab",
                          color: pieces.selected ? "red" : pieces.color
                        }}
                        icon={allPieces[pieces.name]}
                      />
                    </div>
                  ))
                : cell.numberedCell === 0 || cell.numberedCell === 14
                ? "Старт"
                : cell.numberedCell}
            </div>
          </div>
        ))}
        <button
          className="end-turn-button"
          onClick={() => {
            openActionModal({
              showModal: true,
              endTurn: true
            });
          }}
        >
          Завершить ход
        </button>
      </div>
    </>
  );
}
