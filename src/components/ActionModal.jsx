import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Button } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

function ActionModal(props) {
  const {
    changeModal,
    endTurn,
    showActionModal,
    player,
    notification,
    rules,
    whoseTurn
  } = props;
  const dispatch = useDispatch();
  const whitePieces = useSelector((state) => state.whitePieces);
  const blackPieces = useSelector((state) => state.blackPieces);
  const allPieces = useSelector((state) => state.allPieces.chessPieces);
  const [arraySelectedPieces, setArraySelectedPieces] = useState([]);
  const [changeSelected, setChangeSelected] = useState({});
  const [computeWeightFigures, setComputeWeightFigures] = useState({
    totalWeightFigures: 0
  });
  const [errorMessage, setErrorMessage] = useState(false);
  useEffect(() => {
    setComputeWeightFigures({
      totalWeightFigures:
        player === "white"
          ? whitePieces.whitePieces.reduce((sum, curr) => {
              if (curr.selected) {
                sum += curr.weight;
              }
              return sum;
            }, 0)
          : blackPieces.blackPieces.reduce((sum, curr) => {
              if (curr.selected) {
                sum += curr.weight;
              }
              return sum;
            }, 0)
    });
  }, [whitePieces, blackPieces]);

  const namesPieces =
    player === "white"
      ? ["whiteKnight", "whiteBishop", "whiteRook", "whiteQueen", "whiteKing"]
      : ["blackKnight", "blackBishop", "blackRook", "blackQueen", "blackKing"];

  const handleClickByPieces = (pieces) => {
    const computeSelectedWeight = (pieces) => {
      if (pieces === "whitePawn" || pieces === "blackPawn") {
        return 1;
      }
      if (
        pieces === "whiteKnight" ||
        pieces === "whiteBishop" ||
        pieces === "blackKnight" ||
        pieces === "blackBishop"
      ) {
        return 3;
      }
      if (pieces === "whiteRook" || pieces === "blackRook") {
        return 5;
      }
      if (pieces === "whiteQueen" || pieces === "blackQueen") {
        return 9;
      }
      if (pieces === "whiteKing" || pieces === "blackKing") {
        return 12;
      }
    };
    const remainingWeightPieces =
      computeWeightFigures.totalWeightFigures - computeSelectedWeight(pieces);
    if (remainingWeightPieces !== 0) {
      setErrorMessage(true);
    } else {
      setErrorMessage(false);
    }

    setComputeWeightFigures({
      totalWeightFigures:
        computeWeightFigures.totalWeightFigures - computeSelectedWeight(pieces)
    });
    setArraySelectedPieces([...arraySelectedPieces, pieces]);
    setChangeSelected({
      ...changeSelected,
      [pieces]: !changeSelected[pieces]
    });
  };

  const handleSelectButtonClick = () => {
    dispatch({
      type: player === "white" ? "removeWhitePieces" : "removeBlackPieces"
    });
    arraySelectedPieces.map((pieces) => {
      dispatch({
        type: player === "white" ? "addWhitePieces" : "addBlackPieces",
        payload: { pieces: pieces }
      });
    });
    setErrorMessage(false);
    setChangeSelected({});
    setArraySelectedPieces([]);
    changeModal({ showModal: false, notification: false });
  };

  const handleCancelClick = () => {
    player === "white"
      ? dispatch({ type: "resetSelectedWhitePieces" })
      : dispatch({ type: "resetSelectedBlackPieces" });
    setErrorMessage(false);
    setArraySelectedPieces([]);
    changeModal({ showModal: false, notification: false });
    setChangeSelected({});
  };

  const handleTurnButton = (response) => {
    if (response === "yes") {
      dispatch({ type: "resetSelectedWhitePieces" });
      dispatch({ type: "resetSelectedBlackPieces" });
      changeModal({
        showModal: false,
        endTurn: true,
        transitionPlayerTurn: true
      });
    }
    changeModal({ showModal: false, endTurn: false });
  };

  const canSelect = (pieces) => {
    if (player === "white") {
      if (
        (pieces === "whiteBishop" || pieces === "whiteKnight") &&
        computeWeightFigures.totalWeightFigures >= 3
      ) {
        return false;
      } else if (
        pieces === "whiteRook" &&
        computeWeightFigures.totalWeightFigures >= 5
      ) {
        return false;
      } else if (
        pieces === "whiteQueen" &&
        computeWeightFigures.totalWeightFigures >= 9
      ) {
        return false;
      } else if (
        pieces === "whiteKing" &&
        computeWeightFigures.totalWeightFigures >= 12
      ) {
        return false;
      } else {
        return true;
      }
    } else {
      if (
        (pieces === "blackBishop" || pieces === "blackKnight") &&
        computeWeightFigures.totalWeightFigures >= 3
      ) {
        return false;
      } else if (
        pieces === "blackRook" &&
        computeWeightFigures.totalWeightFigures >= 5
      ) {
        return false;
      } else if (
        pieces === "blackQueen" &&
        computeWeightFigures.totalWeightFigures >= 9
      ) {
        return false;
      } else if (
        pieces === "blackKing" &&
        computeWeightFigures.totalWeightFigures >= 12
      ) {
        return false;
      } else {
        return true;
      }
    }
  };

  return (
    <div className={showActionModal ? "modal active" : "modal"}>
      <div
        className={showActionModal ? "modal__content active" : "modal__content"}
      >
        {rules && (
          <div className="rules">
            <h3 className="rules__main-text">
              Для того что бы начать, бросте кубик. (Нажмите кнопку "Бросить
              кубик") Добавить фигуру можно только если выпадет число шесть на
              кубике. Станет активна кнопка "добавить фигуру" и вы сможете
              добавить фигуру пешку. Фигуры добавляются на клетку "0" для белого
              игрока и клетку 14 для черного. Когда у вас уже есть фигуры на
              поле,любую из них (свою) можно двигать (только вперёд, движение
              назад запрещено) на колличество клеток выпавшее на кубике. Фигуры
              двигаются только по периметру доски. Каждая фигура имеет свой
              "вес" и он указан в низу в разделе справочник. В зависимости от
              веса и количесва фигуры можно менять. (Кнопка "Обменять фигуры"
              станет активна когда фигурыи их "вес" будет достаточным чтобы их
              можно было обменять) Фигуры могут устраивать ловушки для фигур
              соперника. Для того что бы "поймать" фигуры соперника, между
              вашими фигурами должно быть не больше шести клеток. Внутри ловушки
              двигаться можно, но нельзя двигаться за её пределы. Что бы
              "выбраться" из ловушки, нужно встать на клетку с фигурой соперника
              которая создает ловушку. Когда ваша фигура становится на клетку с
              фигурой соперника, сравнивается "вес" фигур. Если ваша фигура
              "тяжелее" фигуры стоящей на клетке, то вы уничтожаете фигуру
              соперника. Если ваша фигура легче фигуры соперника стоящей на этой
              клетке, то ваша фигура уничтожается, а вес фиругы соперника
              "перерасчитывается". (На клетке появятся фигуры на раницу в "весе"
              между фигурой соперника и вашей вигурой) Побеждает игрок который
              первый установит на доску фигуру короля.
              <span className="rules__directory">Справочник.</span>
              <span className="rules__weight-pieces">
                Вес фигур: пешка - 1, конь/слон - 3, ладья - 5, ферзь - 9.{" "}
              </span>
            </h3>
            <Button
              className="modal-action-button"
              onClick={() => {
                changeModal({
                  showModal: false,
                  changeRule: false
                });
              }}
            >
              Ок
            </Button>
          </div>
        )}
        {notification && (
          <div className="notification">
            {notification === `change pieces` ? (
              <div className="selection-form">
                <h3 className="notification-text">
                  На какую фигуру вы хотите обменять выбранные фигуры?
                </h3>
                <div className="pieces-list">
                  {namesPieces.map((pieces, index) => (
                    <button
                      disabled={canSelect(pieces)}
                      key={`${pieces}_${index}`}
                      className="btn btn-otline-success"
                    >
                      <FontAwesomeIcon
                        className="chess-pieces pieces-to-choose"
                        onClick={() => {
                          handleClickByPieces(pieces);
                        }}
                        style={{
                          color: changeSelected[pieces]
                            ? "green"
                            : player === "black"
                            ? "black"
                            : "white"
                        }}
                        icon={allPieces[pieces]}
                      />
                    </button>
                  ))}
                </div>
                {!errorMessage || (
                  <span className="error-message">
                    "Вес" фигур которые вы хотите обменять больше тех на которые
                    вы меняете. Если нажмете "обменять", вы потеряете одну или
                    несколько фигур.
                  </span>
                )}
                <div className="button-block">
                  {" "}
                  <Button
                    className="btn btn-success modal-action-button small-text"
                    disabled={arraySelectedPieces.length === 0}
                    onClick={() => {
                      handleSelectButtonClick();
                    }}
                  >
                    Обменять
                  </Button>
                  <Button
                    className="btn btn-danger modal-action-button small-text"
                    onClick={() => {
                      handleCancelClick();
                    }}
                  >
                    Я передумал
                  </Button>
                </div>
              </div>
            ) : (
              <div className="simple-notification-block">
                <h3 className="notification-text">{notification}</h3>
                <Button
                  className="btn btn-success modal-action-button"
                  onClick={() => {
                    changeModal({
                      showModal: false,
                      notification: false
                    });
                  }}
                >
                  Ок
                </Button>
              </div>
            )}
          </div>
        )}
        {endTurn && (
          <div className="modal-action__end-turn">
            <h3 className="end-turn-header">
              Вы действительно хотите завершить ход?
            </h3>{" "}
            <div className="buttons-group">
              <Button
                className="modal-action-button"
                variant="success"
                onClick={() => {
                  handleTurnButton("yes");
                }}
              >
                Да
              </Button>
              <Button
                className="modal-action-button"
                variant="danger"
                onClick={() => {
                  handleTurnButton("no");
                }}
              >
                Нет
              </Button>
            </div>
          </div>
        )}
        {whoseTurn && (
          <div className="modal-action__player-turn">
            <h3 className="player-turn-heder">
              Ход {props.player === "white" ? "белого" : "черного"} игрока
            </h3>
            <Button
              className="modal-action-button"
              variant="success"
              onClick={() => {
                changeModal({
                  showModal: false,
                  whoseTurn: false
                });
              }}
            >
              ОК
            </Button>
          </div>
        )}
      </div>
    </div>
  );
}

export default ActionModal;
