const NoTrap = (
  player,
  whitePieces,
  blackPieces,
  currentPieces,
  diceNumber
) => {
  const allPiecesTrap = [];
  player === "black"
    ? whitePieces.whitePieces.map((pieces) =>
        allPiecesTrap.push(pieces.numberedCell)
      )
    : blackPieces.blackPieces.map((pieces) =>
        allPiecesTrap.push(pieces.numberedCell)
      );
  const piecesTrap = [...new Set(allPiecesTrap)].sort((a, b) => a - b);
  const nearestLowerNumber = [];

  for (let i = 0; i < piecesTrap.length; i++) {
    if (
      piecesTrap.length > 1 &&
      ((currentPieces.numberedCell <= 6 &&
        piecesTrap[0] > currentPieces.numberedCell) ||
        (currentPieces.numberedCell >= 22 &&
          piecesTrap[piecesTrap.length - 1] < currentPieces.numberedCell)) &&
      6 >= piecesTrap[i] + (27 - piecesTrap[piecesTrap.length - 1])
    ) {
      nearestLowerNumber.push(piecesTrap[piecesTrap.length - 1], piecesTrap[0]);
      break;
    }
    if (
      piecesTrap[i] > currentPieces.numberedCell &&
      currentPieces.numberedCell > piecesTrap[i - 1] &&
      (piecesTrap[i] - piecesTrap[i - 1] <= 7 ||
        ((currentPieces.numberedCell <= 6 ||
          currentPieces.numberedCell >= 22) &&
          6 >= piecesTrap[i] + (27 - piecesTrap[piecesTrap.length - 1])))
    ) {
      nearestLowerNumber.push(piecesTrap[i - 1], piecesTrap[i]);
      break;
    }
  }

  const canMoveInsideTrap = () => {
    if (
      nearestLowerNumber[0] < nearestLowerNumber[1] &&
      nearestLowerNumber[1] - currentPieces.numberedCell >= diceNumber
    ) {
      return true;
    }
    if (
      nearestLowerNumber[0] > nearestLowerNumber[1] &&
      currentPieces.numberedCell <= 27
    ) {
      if (
        nearestLowerNumber[1] + 1 >=
        27 - currentPieces.numberedCell + diceNumber
      ) {
        return true;
      }
    }
    if (
      nearestLowerNumber[0] > nearestLowerNumber[1] &&
      currentPieces.numberedCell <= 5
    ) {
      if (currentPieces.numberedCell + diceNumber <= nearestLowerNumber[1]) {
        return true;
      }
    }
  };
  return nearestLowerNumber.length !== 2 || canMoveInsideTrap();
};

module.exports = { NoTrap };
