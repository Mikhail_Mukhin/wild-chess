const ComputeRemainingWeight = (remainingWeight, player, dispatch) => {
  function addPawn() {
    return dispatch({
      type: player === "white" ? "addBlackPieces" : "addWhitePieces",
      payload: {
        pieces: player === "white" ? "blackPawn" : "whitePawn"
      }
    });
  }

  function addKnight() {
    dispatch({
      type: player === "white" ? "addBlackPieces" : "addWhitePieces",
      payload: {
        pieces: player === "white" ? "blackKnight" : "whiteKnight"
      }
    });
  }

  function addRock() {
    dispatch({
      type: player === "white" ? "addBlackPieces" : "addWhitePieces",
      payload: {
        pieces: player === "white" ? "blackRook" : "whiteRook"
      }
    });
  }

  if (remainingWeight === 2) {
    addPawn();
    addPawn();
  }

  if (remainingWeight === 3) {
    addKnight();
  }

  if (remainingWeight === 4) {
    addKnight();
    addPawn();
  }

  if (remainingWeight === 6) {
    addRock();
    addPawn();
  }

  if (remainingWeight === 8) {
    addRock();
    addKnight();
  }
};

module.exports = { ComputeRemainingWeight };
