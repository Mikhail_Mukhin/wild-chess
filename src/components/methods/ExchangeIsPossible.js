const ExchangeIsPossible = (
  player,
  combineIdenticalWhitePieces,
  combineIdenticalBlackPieces,
  whitePieces,
  blackPieces
) => {
  if (player === "white") {
    const AllKeys = Object.keys(combineIdenticalWhitePieces);
    if (!whitePieces.whitePieces.length) {
      return false;
    } else if (whitePieces.whitePieces.length === 1) {
      return false;
    } else if (
      AllKeys.toString() === "whitePawn" &&
      AllKeys.length === 1 &&
      (combineIdenticalWhitePieces.whitePawn === 1 ||
        combineIdenticalWhitePieces.whitePawn === 2)
    ) {
      return false;
    } else if (
      AllKeys.length === 2 &&
      AllKeys.some(
        (key) =>
          key === "whitePawn" || key === "whiteBishop" || key === "whiteKnight"
      ) &&
      AllKeys.some((key) => key === "whiteRook") &&
      (combineIdenticalWhitePieces.whitePawn === 1 ||
        combineIdenticalWhitePieces.whitePawn === 2 ||
        combineIdenticalWhitePieces.whiteBishop === 1 ||
        combineIdenticalWhitePieces.whiteKnight === 1)
    ) {
      return false;
    } else if (
      (AllKeys.length === 2 ||
        AllKeys.every((key) => AllKeys.length === 3 && key !== "whiteRook")) &&
      AllKeys.some((key) => key === "whitePawn") &&
      (AllKeys.some((key) => key === "whiteBishop") ||
        AllKeys.some((key) => key === "whiteKnight")) &&
      combineIdenticalWhitePieces.whitePawn === 1
    ) {
      return false;
    } else if (
      (AllKeys.length === 2 &&
        AllKeys.some((key) => key === "whitePawn") &&
        AllKeys.some((key) => key === "whiteQueen") &&
        (combineIdenticalWhitePieces.whitePawn === 1 ||
          combineIdenticalWhitePieces.whitePawn === 2)) ||
      (AllKeys.length === 1 && AllKeys.some((key) => key === "whiteQueen"))
    ) {
      return false;
    } else {
      return true;
    }
  }

  if (player === "black") {
    const AllKeys = Object.keys(combineIdenticalBlackPieces);
    if (!blackPieces.blackPieces.length) {
      return false;
    } else if (blackPieces.blackPieces.length === 1) {
      return false;
    } else if (
      AllKeys.toString() === "blackPawn" &&
      AllKeys.length === 1 &&
      (combineIdenticalBlackPieces.blackPawn === 1 ||
        combineIdenticalBlackPieces.blackPawn === 2)
    ) {
      return false;
    } else if (
      AllKeys.length === 2 &&
      AllKeys.some(
        (key) =>
          key === "blackPawn" || key === "blackBishop" || key === "blackKnight"
      ) &&
      AllKeys.some((key) => key === "blackRook") &&
      (combineIdenticalBlackPieces.blackPawn === 1 ||
        combineIdenticalBlackPieces.blackPawn === 2 ||
        combineIdenticalBlackPieces.blackBishop === 1 ||
        combineIdenticalBlackPieces.blackKnight === 1)
    ) {
      return false;
    } else if (
      (AllKeys.length === 2 ||
        AllKeys.every((key) => AllKeys.length === 3 && key !== "blackRook")) &&
      AllKeys.some((key) => key === "blackPawn") &&
      (AllKeys.some((key) => key === "blackBishop") ||
        AllKeys.some((key) => key === "blackKnight")) &&
      combineIdenticalBlackPieces.blackPawn === 1
    ) {
      return false;
    } else if (
      (AllKeys.length === 2 &&
        AllKeys.some((key) => key === "blackPawn") &&
        AllKeys.some((key) => key === "blackQueen") &&
        (combineIdenticalBlackPieces.blackPawn === 1 ||
          combineIdenticalBlackPieces.blackPawn === 2)) ||
      (AllKeys.length === 1 && AllKeys.some((key) => key === "blackQueen"))
    ) {
      return false;
    } else {
      return true;
    }
  }
};

module.exports = { ExchangeIsPossible };
