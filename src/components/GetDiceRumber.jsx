import React, { Component } from "react";
import ReactDice from "react-dice-complete";
import "react-dice-complete/dist/react-dice-complete.css";
import { Button } from "react-bootstrap";
import { connect } from "react-redux";

class GetDiceRumber extends Component {
  constructor() {
    super();
    this.state = { diceThrown: false };
  }

  componentDidUpdate(prevProps) {
    if (prevProps.player !== this.props.player) {
      this.setState({
        diceThrown: false
      });
    }
  }

  rollAll() {
    this.reactDice.rollAll();
    this.setState({
      diceThrown: true
    });
  }

  getDiceRoll(num) {
    const { dispatch } = this.props;
    dispatch({ type: "saveDiceNumber", payload: num });
  }

  render() {
    return (
      <div className="dice-roll__block">
        <Button
          variant="success"
          className="roll-button"
          disabled={this.state.diceThrown}
          onClick={() => this.rollAll()}
        >
          Бросить кубик
        </Button>
        <ReactDice
          numDice={1}
          faceColor="hsla(120, 100%, 50%, 1)"
          dotColor="rgb(0,0,0)"
          dieSize="100"
          outline={true}
          rollTime="2"
          rollDone={(num) => this.getDiceRoll(num)}
          disableIndividual="true"
          ref={(dice) => (this.reactDice = dice)}
        />
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return { dispatch };
};

export default connect(null, mapDispatchToProps)(GetDiceRumber);
