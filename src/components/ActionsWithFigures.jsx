import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { ExchangeIsPossible } from "./methods/ExchangeIsPossible";

function ActionsWithFigures(props) {
  const { turn, player, notificationMessage, rulesGame } = props;
  const dispatch = useDispatch();
  const diceNumber = useSelector((state) => state.diceNumber.diceNumber);
  const [firstMove, setFirstMove] = useState(true);
  const [alreadyAdded, setalreadyAdded] = useState(false);
  const [chessPieces, setChessPieces] = useState({});
  const [notificationForWhitePlayer, setNotificationForWhitePlayer] = useState({
    piecesOnStart: true,
    aboutExchange: true
  });
  const [notificationForBlackPlayer, setNotificationForBlackPlayer] = useState({
    piecesOnStart: true,
    aboutExchange: true
  });
  const allPieces = useSelector((state) => state.allPieces.chessPieces);
  const whitePieces = useSelector((state) => state.whitePieces);
  const blackPieces = useSelector((state) => state.blackPieces);
  const hasWhiteSelectedPieces = whitePieces.whitePieces.some(
    (pieces) => pieces.selected
  );
  const hasBlackSelectedPieces = blackPieces.blackPieces.some(
    (pieces) => pieces.selected
  );

  const whitePiecesOnStartCell = whitePieces.whitePieces.length
    ? whitePieces.whitePieces.reduce((sum, curr) => {
        if (curr.coordinates.i === 0 && curr.coordinates.k === 0) {
          sum++;
        }
        return sum;
      }, 0)
    : "";
  const blackPiecesOnStartCell = blackPieces.blackPieces.length
    ? blackPieces.blackPieces.reduce((sum, curr) => {
        if (curr.coordinates.i === 7 && curr.coordinates.k === 7) {
          sum++;
        }
        return sum;
      }, 0)
    : "";

  const combineIdenticalWhitePieces = whitePieces.whitePieces.length
    ? whitePieces.whitePieces.reduce((sum, curr) => {
        sum[curr.name] = (sum[curr.name] || 0) + 1;
        return sum;
      }, {})
    : "";

  const combineIdenticalBlackPieces = blackPieces.blackPieces.length
    ? blackPieces.blackPieces.reduce((sum, curr) => {
        sum[curr.name] = (sum[curr.name] || 0) + 1;
        return sum;
      }, {})
    : "";

  const addPiecesToWhitePlayer = (pieces) => {
    if (whitePiecesOnStartCell < 4) {
      dispatch({ type: "addWhitePieces", payload: { pieces: pieces } });
    }
  };
  const addPiecesToBlackPlayer = (pieces) => {
    if (blackPiecesOnStartCell < 4) {
      dispatch({ type: "addBlackPieces", payload: { pieces: pieces } });
    }
  };

  const addPawn = () => {
    setalreadyAdded(true);
    setFirstMove(false);
    if (
      (combineIdenticalWhitePieces.whitePawn === 8 && player === "white") ||
      (combineIdenticalBlackPieces.blackPawn === 8 && player === "black")
    ) {
      notificationMessage(
        `Допустимо максимум 8 пешек. Что бы добавить новую, обменяйте 
        их на более "тяжёлую" фигуру.`
      );
      return;
    } else {
      player === "white"
        ? addPiecesToWhitePlayer("whitePawn")
        : addPiecesToBlackPlayer("blackPawn");
    }
  };

  const changePiecesDispatch = () => {
    if (
      (notificationForWhitePlayer.aboutExchange && player === "white") ||
      (notificationForBlackPlayer.aboutExchange && player === "black")
    ) {
      notificationMessage(
        `Что бы обменять фигуры на более "тяжелые", нажмите кнопку "обменять фигуры", 
      потом на фигуры которые хотите обменять, затем снова на кнопку "обменять 
      фигуры". Во всплывающем окне вам будут предложены варианты для обмена.`
      );
    } else {
      dispatch({ type: "chengePieces" });
    }
    if (player === "white") {
      setNotificationForWhitePlayer({
        ...notificationForWhitePlayer,
        aboutExchange: false
      });
    }
    if (player === "black") {
      setNotificationForBlackPlayer({
        ...notificationForBlackPlayer,
        aboutExchange: false
      });
    }

    if (
      (player === "white" &&
        ExchangeIsPossible(
          player,
          combineIdenticalWhitePieces,
          combineIdenticalBlackPieces,
          whitePieces,
          blackPieces
        ) &&
        hasWhiteSelectedPieces) ||
      (player === "black" &&
        ExchangeIsPossible(
          player,
          combineIdenticalWhitePieces,
          combineIdenticalBlackPieces,
          whitePieces,
          blackPieces
        ) &&
        hasBlackSelectedPieces)
    ) {
      notificationMessage(`change pieces`);
    }
  };

  useEffect(() => {
    if (
      (whitePiecesOnStartCell === 4 &&
        player === "white" &&
        notificationForWhitePlayer.piecesOnStart) ||
      (blackPiecesOnStartCell === 4 &&
        player === "black" &&
        notificationForBlackPlayer.piecesOnStart)
    ) {
      notificationMessage(
        `Допустимо не больше четырех фигур на клетке "старт". Если вы захотите 
        добавить еще фигур, передвигайте их на другие клетки или 
        обменять те что у вас есть, на более "тяжёлые".`
      );
    }

    if (player === "white" && whitePiecesOnStartCell === 4) {
      setNotificationForWhitePlayer({
        ...notificationForWhitePlayer,
        piecesOnStart: false
      });
    }
    if (player === "black" && blackPiecesOnStartCell === 4) {
      setNotificationForBlackPlayer({
        ...notificationForBlackPlayer,
        piecesOnStart: false
      });
    }
  }, [whitePiecesOnStartCell, blackPiecesOnStartCell]);

  useEffect(() => {
    setalreadyAdded(false);
  }, [turn]);

  return (
    <div className="info-field">
      <div className="all-content">
        <h3 className="players-move">
          Ход {turn === "white" ? "белого" : "черного"} игрока.
          {firstMove}
        </h3>
        <div className="button-block">
          <button
            disabled={
              !firstMove &&
              (diceNumber !== 6 ||
                alreadyAdded ||
                hasWhiteSelectedPieces ||
                ((chessPieces.whitePawn === 8 ||
                  whitePiecesOnStartCell === 4) &&
                  player === "white") ||
                hasBlackSelectedPieces ||
                ((chessPieces.blackPawn === 8 ||
                  blackPiecesOnStartCell === 4) &&
                  player === "black"))
            }
            className="btn add-pieces-button"
            onClick={addPawn}
          >
            добавить фигуру
          </button>
          <button
            disabled={
              !ExchangeIsPossible(
                player,
                combineIdenticalWhitePieces,
                combineIdenticalBlackPieces,
                whitePieces,
                blackPieces
              )
            }
            className="btn change-pieces-button"
            onClick={changePiecesDispatch}
          >
            обменять фигуры
          </button>
        </div>

        <div className="white-player-pieces">
          Фигуры белого игрока:
          {Object.entries(combineIdenticalWhitePieces).map((pieces) => (
            <div key={pieces}>
              <FontAwesomeIcon
                style={{ color: "white" }}
                icon={allPieces[pieces[0]]}
              />
              x {[pieces[1]]}
            </div>
          ))}
        </div>
        <div className="white-player-pieces">
          Фигуры чёрного игрока:
          {Object.entries(combineIdenticalBlackPieces).map((pieces) => (
            <div key={pieces}>
              <FontAwesomeIcon
                style={{ color: "black" }}
                icon={allPieces[pieces[0]]}
              />
              x {[pieces[1]]}
            </div>
          ))}
        </div>
      </div>

      <button className="btn rules-button" onClick={rulesGame}>
        посмотреть правила игры.
      </button>
    </div>
  );
}

export default ActionsWithFigures;
